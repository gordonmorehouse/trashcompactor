trashcompactor
==============

Browser extension which collapses links to content mills, clickbait sites, and gutter journalism farms. Allows the user a chance to rethink clicking trash links.

Click once to expand the little trash can so the link is readable; click again to follow it.
